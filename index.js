// https://betterprogramming.pub/creating-custom-eslint-rules-cdc579694608
// https://astexplorer.net/
module.exports = {
  rules: {
    "no-async-delay": {
      meta: {
        type: "problem",
        docs: {
          description:
            "The funcion/util 'asyncDelay is using on the app. It must not pushed to remote git",
        },
        messages: {
          match:
            "Found '{{filename}}' file, a call to funcion 'asyncDelay()'. It cand be very dangerous if the call is pushed on remote git.",
        },
      },
      create: (context) => ({
        CallExpression(node) {
          if (node.callee.name === 'asyncDelay') {
            context.report({
              node,
              message: 'function "asyncDelay(), must not pushed on git remote',
            });
          }
        }
      }),
    },
  },
};
